<%--
  Created by IntelliJ IDEA.
  User: Diamondra
  Date: 09/11/2022
  Time: 08:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Formulaire</title>
</head>
<body>
    <form action="emp-get.do" method="post">
        <label>Nom</label>
        <input name="nom" type="text" autocomplete="off">
        <label>Age</label>
        <input name="age" type="number" autocomplete="off">
        <label>Taille</label>
        <input name="taille" type="number" step="0.01" autocomplete="off">
        <input type="submit" value="Enregistrer">
    </form>
    <a href="emp-list.do">Voir liste</a>
</body>
</html>
