<%--
  Created by IntelliJ IDEA.
  User: Diamondra
  Date: 28/10/2022
  Time: 15:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="model.Employe" %>
<%@ page import="java.util.List" %>

<html>
<head>
    <title>Liste</title>
</head>
<body>
<% List<Employe> employes = (List<Employe>) request.getAttribute("emp-list"); %>

    <table>
        <tr>
            <th>Nom</th>
            <th>Age</th>
            <th>Taille</th>
        </tr>
        <% for (Employe emp : employes) { %>
        <tr>
            <td><%=emp.getNom() %></td>
            <td><%=emp.getAge() %></td>
            <td><%=emp.getTaille() %></td>
        </tr>
        <%  } %>
    </table>
</body>
</html>
