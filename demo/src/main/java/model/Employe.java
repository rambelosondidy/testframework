package model;

import annotation.MethodMapping;
import util.ModelView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Employe {
    int age;
    String nom;
    double taille;
    static List<Employe> list = new ArrayList<>();

    public Employe() {
    }

    
    public Employe(String nom, int age,  double taille) {
        this.age = age;
        this.nom = nom;
        this.taille = taille;
    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getTaille() {
        return taille;
    }

    public void setTaille(double taille) {
        this.taille = taille;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    @MethodMapping(url = "emp-list")
    public ModelView list()
    {
        ModelView modelView = new ModelView();
        modelView.setJspFile("emp-list.jsp");
        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("emp-list", list);
        modelView.setData(data);
        return modelView;
    }

    @MethodMapping(url = "emp-get")
    public ModelView get()
    {
        ModelView modelView = new ModelView();
        modelView.setJspFile("emp-detail.jsp");
        HashMap<String, Object> data = new HashMap<String, Object>();
        list.add(this);
        data.put("emp-detail", this);
        modelView.setData(data);
        return modelView;

    }
}
